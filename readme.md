# Webpack starter

### Features
* sass/scss to css compiling,
* js transpiling (polyfills included),
* sasslint,
* moving fonts and images to public directory,
* dev server (by default on port 3000),
* sourcemaps are included,
* bootstrap grid is added

### Commands
1. _npm_ _run_ _dev_ launches dev server on port 3000
2. _npm_ _run_ _build_ builds assets without minification
3. _npm_ _run_ _build-prod_ builds assets for production
