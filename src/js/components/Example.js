export default class Example {
  constructor(mesage ='Hello World'){
    this.message = mesage;
    this.sayHello();
  }
  sayHello(){
    console.log(this.message);
  }
}